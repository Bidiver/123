﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp8
{
    public partial class Form2 : Form
    {
        string sql_conn = @"Data Source=LAB206-11\SQLEXPRESS;Initial Catalog=Cveti;Integrated Security=True";
        public Form2()
        {
            InitializeComponent();
        }

        string a = "";

        private void Form2_Load(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(sql_conn);

                string select = "select * from [Cveti].[dbo].[Tovar]";

                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand(select, sqlConnection);
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                DataTable dataTable = new DataTable();

                sqlDataAdapter.Fill(dataTable);

                listBox1.DataSource = dataTable;
                listBox1.DisplayMember = "Name";
                dataGridView1.DataSource = dataTable;


                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dataGridView1.Rows[i].Cells[4].Value) >= 15)
                    {
                        dataGridView1.Rows[i].Cells[4].Style.BackColor = Color.Chartreuse;
                    }
                }
                a = Convert.ToString(dataGridView1.Rows.Count);
                string b = Convert.ToString(dataGridView1.Rows.Count);
                label3.Text = $"{b} из {a} записей";
            }
            catch
            {
                MessageBox.Show("Ошибка", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(sql_conn);

                string select = "Select * from [Cveti].[dbo].[Tovar] where Name Like '" + textBox1.Text + "%'";

                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand(select, sqlConnection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
                DataTable dataTable = new DataTable();

                dataAdapter.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dataGridView1.Rows[i].Cells[4].Value) >= 15)
                    {
                        dataGridView1.Rows[i].Cells[4].Style.BackColor = Color.Chartreuse;
                    }
                }
                string b = Convert.ToString(dataGridView1.Rows.Count);
                label3.Text = $"{b} из {a} записей";
            }
            catch
            {
                MessageBox.Show("Ошибка", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = "";
                textBox2.Text = "";
                SqlConnection sqlConnection = new SqlConnection(sql_conn);

                string select = "select * from [Cveti].[dbo].[Tovar]";

                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand(select, sqlConnection);
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                DataTable dataTable = new DataTable();

                sqlDataAdapter.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dataGridView1.Rows[i].Cells[4].Value) >= 15)
                    {
                        dataGridView1.Rows[i].Cells[4].Style.BackColor = Color.Chartreuse;
                    }
                }
                string b = Convert.ToString(dataGridView1.Rows.Count);
                label3.Text = $"{b} из {a} записей";
            }
            catch
            {
                MessageBox.Show("Ошибка", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(sql_conn);

                string select = "SELECT * FROM [Cveti].[dbo].[Tovar] ORDER BY Stoimost DESC";

                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand(select, sqlConnection);
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                DataTable dataTable = new DataTable();

                sqlDataAdapter.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dataGridView1.Rows[i].Cells[4].Value) >= 15)
                    {
                        dataGridView1.Rows[i].Cells[4].Style.BackColor = Color.Chartreuse;
                    }
                }
                string b = Convert.ToString(dataGridView1.Rows.Count);
                label3.Text = $"{b} из {a} записей";
            }
            catch
            {
                MessageBox.Show("Ошибка", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }   
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(sql_conn);

                string select = "SELECT * FROM [Cveti].[dbo].[Tovar] ORDER BY Stoimost";

                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand(select, sqlConnection);
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                DataTable dataTable = new DataTable();

                sqlDataAdapter.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dataGridView1.Rows[i].Cells[4].Value) >= 15)
                    {
                        dataGridView1.Rows[i].Cells[4].Style.BackColor = Color.Chartreuse;
                    }
                }
                string b = Convert.ToString(dataGridView1.Rows.Count);
                label3.Text = $"{b} из {a} записей";
            }
            catch
            {
                MessageBox.Show("Ошибка", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(listBox1.SelectedValue) != "")
                {
                    pictureBox1.ImageLocation = $"C:\\Users\\Admin11\\Desktop\\1904\\WindowsFormsApp8\\Resources\\{Convert.ToString(listBox1.SelectedValue)}";
                    pictureBox1.Load();
                }
                else
                {
                    pictureBox1.ImageLocation = "C:\\Users\\Admin11\\Desktop\\1904\\WindowsFormsApp8\\Resources\\picture.png";
                    pictureBox1.Load();
                }
            }
            catch
            {
                MessageBox.Show("Ошибка", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(sql_conn);

                string select = "Select * from [Cveti].[dbo].[Tovar] where Stoimost Like '" + textBox2.Text + "%'";

                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand(select, sqlConnection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCommand);
                DataTable dataTable = new DataTable();

                dataAdapter.Fill(dataTable);

                dataGridView1.DataSource = dataTable;
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (Convert.ToInt32(dataGridView1.Rows[i].Cells[4].Value) >= 15)
                    {
                        dataGridView1.Rows[i].Cells[4].Style.BackColor = Color.Chartreuse;
                    }
                }
                string b = Convert.ToString(dataGridView1.Rows.Count);
                label3.Text = $"{b} из {a} записей";
            }
            catch
            {
                MessageBox.Show("Ошибка", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
